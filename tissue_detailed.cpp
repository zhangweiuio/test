#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>

#include <omp.h>
#include "RectPart2D.h"
#include <malloc.h> 
#include <stdlib.h>

#define global_N_x 500
#define global_N_y 500
#define Nryr  100 
#define Ndhpr 15
#define Ndyads 100
#define Ntotdyads 10000
#define beats 2

using namespace std;

double t,dt,d_x,Dx,**dtstep;
double dx;
double tstim,stimtime,ist;
int steps,ibeat;
int bcl=1000;

ofstream fout;
ofstream f1out;
ofstream f0out;
ostringstream file;

double **v, **vtemp,**dvdt,**dvdtclock;
double **m,**hf,**hs,**hsp,**j,**jp;
double **ml,**hl,**hlp;
double **nai,**ki,**nass,**kss;

double **ap,**a,**iF,**iS,**iFp,**iSp;
double **xs1,**xs2,**xrs,**xrf,**xk1;
double **d,**ff,**fcaf,**fs,**fcas,**jca,**ffp,**fcafp;

/*
double ***cacyt,***cass,***cads,***cansr,***cajsr;
double ***CaMKt,***trel,***Poryr;
int ****ryrstate,***nactive;
*/

double cacyt[global_N_x][global_N_y][Ndyads],cass[global_N_x][global_N_y][Ndyads],cads[global_N_x][global_N_y][Ndyads],
	cansr[global_N_x][global_N_y][Ndyads],cajsr[global_N_x][global_N_y][Ndyads],nca[global_N_x][global_N_y][Ndyads];
double CaMKt[global_N_x][global_N_y][Ndyads],trel[global_N_x][global_N_y][Ndyads],Poryr[global_N_x][global_N_y][Ndyads];
int ryrstate[global_N_x][global_N_y][Ndyads][Nryr],nactive[global_N_x][global_N_y][Ndyads];

void allocate_array2D (double ***ptr, int Nx, int Ny)
{
  double *base_ptr = (double*)malloc(Nx*Ny*sizeof(double));
  *ptr = (double**)malloc(Nx*sizeof(double*));
  for (int i=0; i<Nx; i++)
    (*ptr)[i] = &(base_ptr[i*Ny]);
}


void all_3D(double ****ptr, int Nx, int Ny)
{
  double *base_ptr = (double*)malloc(Nx*Ny*Ndyads*sizeof(double));
  *ptr = (double***)malloc(Nx*sizeof(double**));
  for (int ix=0; ix<Nx; ix++)
  {
	(*ptr)[ix] = (double**)malloc(Ny*sizeof(double*));
	for (int iy = 0; iy<Ny; iy++)
       {
		(*ptr)[ix][iy] = &(base_ptr[ix*Ny*Ndyads + iy*Ndyads]);
       }
   }
}

void all_4D(int *****ptr, int Nx, int Ny)
{
	int *base_ptr = (int*)malloc(Nx*Ny*Ndyads*Nryr*sizeof(int));
	*ptr =  (int****)malloc(Nx*sizeof(int***));

	for (int ix = 0; ix<Nx; ix++)
	{
		(*ptr)[ix] = (int***)malloc(Ny*sizeof(int**));
		for (int iy = 0; iy<Ny; iy++)
		{
			(*ptr)[ix][iy] = (int**)malloc(Ndyads*sizeof(int*));
			for (int idyads = 0; idyads<Ndyads; idyads++)
			{
				(*ptr)[ix][iy][idyads] = &(base_ptr[ix*Ny*Ndyads*Nryr + iy*Ndyads*Nryr + idyads*Nryr]);
			}
		}
	}
}			

void deallocate_array2D (double ***ptr)
{
  free((*ptr)[0]);
  free(*ptr);
}



void allocate_all_arrays (int Nx, int Ny)
{
  allocate_array2D(&v,Nx,Ny);  
  allocate_array2D(&vtemp,Nx,Ny); memset(vtemp[0],0,Nx*Ny*sizeof(double));
  allocate_array2D(&dvdt,Nx,Ny); 
  allocate_array2D(&dvdtclock,Nx,Ny);
  allocate_array2D(&dtstep,Nx,Ny);
  allocate_array2D(&m,Nx,Ny);
  allocate_array2D(&hf,Nx,Ny);
  allocate_array2D(&hs,Nx,Ny);
  allocate_array2D(&hsp,Nx,Ny);
  allocate_array2D(&j,Nx,Ny);
  allocate_array2D(&jp,Nx,Ny);
  allocate_array2D(&ml,Nx,Ny);
  allocate_array2D(&hl,Nx,Ny);
  allocate_array2D(&hlp,Nx,Ny);
  allocate_array2D(&nai,Nx,Ny);
  allocate_array2D(&ki,Nx,Ny);
  allocate_array2D(&nass,Nx,Ny);
  allocate_array2D(&kss,Nx,Ny);

  allocate_array2D(&ap,Nx,Ny);
  allocate_array2D(&a,Nx,Ny);
  allocate_array2D(&iF,Nx,Ny);
  allocate_array2D(&iS,Nx,Ny);
  allocate_array2D(&iFp,Nx,Ny);
  allocate_array2D(&iSp,Nx,Ny);
  allocate_array2D(&xs1,Nx,Ny);
  allocate_array2D(&xs2,Nx,Ny);
  allocate_array2D(&xrs,Nx,Ny);
  allocate_array2D(&xrf,Nx,Ny);
  allocate_array2D(&xk1,Nx,Ny);

  allocate_array2D(&d,Nx,Ny);
  allocate_array2D(&ff,Nx,Ny);
  allocate_array2D(&fcaf,Nx,Ny);
  allocate_array2D(&fs,Nx,Ny);
  allocate_array2D(&fcas,Nx,Ny);
  allocate_array2D(&jca,Nx,Ny);
  allocate_array2D(&ffp,Nx,Ny);
  allocate_array2D(&fcafp,Nx,Ny);
 
/* 
  all_3D(&nca,Nx,Ny);
  all_3D(&cacyt,Nx,Ny);
  all_3D(&cass,Nx,Ny);
  all_3D(&cansr,Nx,Ny);
  all_3D(&cajsr,Nx,Ny);
  all_3D(&CaMKt,Nx,Ny);
  all_3D(&trel,Nx,Ny);
  all_4D(&ryrstate,Nx,Ny);
*/  
} 


//Random Number Generator
//MTRand mtrand1;
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-12
#define RNMX (1.0-EPS)

struct RandData
{
        long myrandomseed;
        long iy;
        long iv[NTAB];
        double padding[16];
};

double ndrand48(struct RandData * pRD)
{
        int j;
        long k;
        double temp;
        if (pRD->myrandomseed <= 0 || ! pRD->iy)
	{
                if (-(pRD->myrandomseed) < 1)
                        pRD->myrandomseed=1;
                else
                        pRD->myrandomseed = -(pRD->myrandomseed);
                for (j=NTAB+7;j>=0;j--) 
		{
                        k=(pRD->myrandomseed)/IQ;
                        pRD->myrandomseed=IA*(pRD->myrandomseed-k*IQ)-IR*k;
                        if (pRD->myrandomseed < 0)
                                pRD->myrandomseed += IM;
                        if (j < NTAB)
                                pRD->iv[j] = pRD->myrandomseed;
                }
                pRD->iy=pRD->iv[0];
        }

        k=(pRD->myrandomseed)/IQ;
        pRD->myrandomseed=IA*(pRD->myrandomseed-k*IQ)-IR*k;
        if (pRD->myrandomseed < 0)
                pRD->myrandomseed += IM;
        j=pRD->iy/NDIV;
        pRD->iy=pRD->iv[j];
        pRD->iv[j] = pRD->myrandomseed;
        if ((temp=AM*pRD->iy) > RNMX)
                return RNMX;
        else
                return temp;
}

void comp_cell(int ix, int iy, int offset_x, int offset_y, struct RandData * pRD);


int main(int nargs, char** args)
{

	dt = 0.01;
	d_x = 0.5; //dx for voltage
	dx = 0.2;
	Dx = 0.2;
	
	tstim = 100;
	stimtime = 100;

	steps = beats*bcl/dt;


	//***************************************************
	RectPart2D* partitioner;
	int num_procs1=0, num_procs2=0;
	int N_x, N_y;

	MPI_Init (&nargs, &args);
	if (nargs>1)
	  num_procs1 = atoi(args[1]);
	if (nargs>2)
	  num_procs2 = atoi(args[2]);

	partitioner = new RectPart2D(global_N_x,global_N_y,
				     num_procs1,num_procs2);
	N_x = partitioner->sub_N_x();
	N_y = partitioner->sub_N_y();
	allocate_all_arrays(N_x,N_y);
	//***************************************************


	//Files for output
	
	file<<"file_"<<partitioner->my__rank();
	fout.open(file.str().c_str());
	f1out.open("ca_cyt");
	f0out.open("ca_cell");



//	initial conditions
	for (int ix = 1; ix<N_x-1; ix++)
		for (int iy = 1; iy<N_y-1 ; iy++) 
		{				
			v[ix][iy]=-87.6;
			nai[ix][iy]=6.3;
			nass[ix][iy] = nai[ix][iy];
			ki[ix][iy]=145;
			kss[ix][iy] = ki[ix][iy];
			
			for (int idyads = 0; idyads<Ndyads; idyads++)
			{
				cacyt[ix][iy][idyads]=.72e-4;
				cass[ix][iy][idyads]=1e-4;
				cansr[ix][iy][idyads] = 1.44;
				cajsr[ix][iy][idyads] = 1.44;
				CaMKt[ix][iy][idyads] = 0.009;
				nca[ix][iy][idyads] = 0.001;

				for (int iryr = 0; iryr<Nryr; iryr++)
				{
					ryrstate[ix][iy][idyads][iryr] = 0;
				}
			}

			m[ix][iy]=0.01;
			hf[ix][iy]=0.7;
			hs[ix][iy]=0.7;
			j[ix][iy]=0.7;
			hsp[ix][iy]=0.45;
			jp[ix][iy]=.7;
			ml[ix][iy]=0.0002;
			hl[ix][iy]=0.5;
			hlp[ix][iy]=0.27;
			a[ix][iy]=0.001;
			iF[ix][iy]=1;
			iS[ix][iy]=.76;
			ap[ix][iy]=.0005;
			iFp[ix][iy]=1;
			iSp[ix][iy]=.8;
			d[ix][iy]=0;
			ff[ix][iy]=1;
			fs[ix][iy]=.91;
			fcaf[ix][iy]=1;
			fcas[ix][iy]=1;
			jca[ix][iy]=1;
			ffp[ix][iy]=1;
			fcafp[ix][iy]=1;
			xrf[ix][iy]=0;
			xrs[ix][iy]=0.27;
			xs1[ix][iy]=0.25;
			xs2[ix][iy]=0;
			xk1[ix][iy]=1;
				
		}

	#pragma omp parallel
	{
		int thread_id = omp_get_thread_num();
		struct RandData *pRD = (struct RandData *)malloc(sizeof(struct RandData));
		pRD->myrandomseed = -111 - thread_id - partitioner->my__rank();
		pRD->iy = 0;


	for (int icounter = 0; icounter<steps; icounter++)
	{

	#pragma omp for
	for (int ix = 1; ix<N_x-1; ix++) 
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{
			if ((icounter%10 == 0) ||(fabs(dvdt[ix][iy])>0.2)||(dvdtclock[ix][iy]<20) )
			{
			  	comp_cell(ix,iy,
				partitioner->offset_x(),
				partitioner->offset_y(),pRD);
				dtstep[ix][iy] = 0;
			}

		}
	}

	
	#pragma omp for
	for (int ix = 1; ix<N_x-1; ix++)
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{	
			v[ix][iy] += dt*Dx*(vtemp[ix+1][iy] + vtemp[ix-1][iy] + vtemp[ix][iy-1] + vtemp[ix][iy+1] -4*vtemp[ix][iy])
					/(d_x*d_x);
		}
	}

	#pragma omp master
	{
	  int i;
	  partitioner->start_communication(v);

	  // -------- treatment of boundary conditions start ----
	  if (!partitioner->has_lower_neighbor())
	    for (i=1; i<N_x-1; i++)
	      v[i][0] = v[i][1];

	  if (!partitioner->has_upper_neighbor())
	    for (i=1; i<N_x-1; i++)
	      v[i][N_y-1] = v[i][N_y-2];

	  if (!partitioner->has_left_neighbor())
	    for (i=1; i<N_y-1; i++)
	      v[0][i] = v[1][i];

	  if (!partitioner->has_right_neighbor())
	    for (i=1; i<N_y-1; i++)
	      v[N_x-1][i] = v[N_x-2][i];
	  // -------- treatment of boundary conditions end ----
	  partitioner->finish_communication(v);
	}
	#pragma omp barrier

	#pragma omp for
	for (int ix = 0; ix<N_x; ix++)
	{
		for (int iy = 0; iy<N_y; iy++)
		{
			dvdt[ix][iy] = (v[ix][iy] - vtemp[ix][iy])/dt;
			if (dvdt[ix][iy]>1) dvdtclock[ix][iy] = 0;
			dtstep[ix][iy] += dt;
			dvdtclock[ix][iy] += dt;
			vtemp[ix][iy] = v[ix][iy];
		}
	}

	#pragma omp single
	{
	if (t>=tstim && t<(tstim+dt))
	{
		stimtime = 0;
		ibeat = ibeat+1;
		tstim = tstim+bcl;
		if (partitioner->is_master_proc())
		  cout<<ibeat<<endl;
	}
	
	if (stimtime>=0 && stimtime<=0.5)
		ist = -80;
	else
		ist = 0;

//	if (ibeat>beats-7) {ist = 0; Dx=0;}

	if (icounter%1000 == 0) //  && partitioner->is_master_proc())
	{
		for (int iy = 1; iy<N_y-1; iy++)
		{
			for (int ix = 1; ix<N_x-1; ix++)
			{
				fout<<v[ix][iy]<<"	";
				
			}
			fout<<endl;
		}


/*		if (partitioner->my__rank() == 5)
		{
			for (int idyad = 0; idyad<Ndyads; idyad++)
			{
				f1out<<cacyt[N_x/2][N_y/2][idyad]<<"	";
			}
			f1out<<endl;
		}			
*/
	}

	
	t+=dt;
	stimtime += dt;
	}//end single region
	}//end time loop
	} //end parallel region

	MPI_Finalize ();
	return 0;
}



void comp_cell(int ix, int iy, int offset_x, int offset_y, struct RandData * pRD)
{

int Nx = 100; int Ny=1; int Nz=1;
int Nvox = 1;

double nao = 140;
double ko = 5.4;
double cao = 1.8;
double iso = 0;
double gna = 75;
double mtau,hftau,hstau,hsptau,jtau,jptau,Ahf,Ahs;
double mss,hss,jss,hssp;
double h,hp,finap;
double CaMKa_cell;
double mlss,mltau,hlss,hlssp,hltau,hlptau;

double gnal,finalp;
double naiont,caiont,kiont,it,istim;
double ina,inal,INab,ilca,icab,icat,ipca,ilcana,INaK,inaca,inacass,Ito,IKr,IKs,IK1,IKb,ilcak;
double CamKa_cell,CaMKb_cell,CaMKt_cell;
double nsr, jsr, cadyad, casub,cai;

double dx = 0.2;
double dy = 0.2;
double dz = 0.2;

double R = 8314;
double temp = 310;
double frdy = 96485;
double ena;

double casstemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cansrtemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cacyttemp[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cassdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cansrdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];
double cacytdiff[((Nx-1)*Nvox+1)*((Ny-1)*Nvox+1)*((Nz-1)*Nvox+1)];

int Nx_diff = (Nx-1)*Nvox + 1;
int Ny_diff = (Ny-1)*Nvox + 1;
int Nz_diff = (Nz-1)*Nvox + 1;

//diffusion coefficients
double Dsr = 0.02;
double Dcyt = 0.2;

//buffers
double kmcsqn = 1;
double csqnbar = 10;
double bsrbar = 0.047;
double	bslbar = 1.124;
double	kmbsr = 0.00087;
double	kmbsl = 0.0087;
double trpnbar = 0.07;
double kmtrpn = 0.0005;
double cmdnbar = 0.05;
double kmcmdn = 0.00238;

//camkinase
double const aCaMK=0.05;
double const bCaMK=0.00068;
double const CaMKo=0.05;
double const KmCaM=0.0015;
double const KmCaMK=0.15;
double CaMKa,CaMKb;

double dt = dtstep[ix][iy];

if (iso == 1) gna = 2.7*gna;
mtau = 1.0/(6.765*exp((v[ix][iy]+11.64)/34.77)+8.552*exp(-(v[ix][iy]+77.42)/5.955));
hftau=1.0/(1.432e-5*exp(-(v[ix][iy]+1.196)/6.285)+6.149*exp((v[ix][iy]+0.5096)/20.27));
hstau=1.0/(0.009794*exp(-(v[ix][iy]+17.95)/28.05)+0.3343*exp((v[ix][iy]+5.730)/56.66));
hsptau=3.0*hstau;
jtau=2.038+1.0/(0.02136*exp(-(v[ix][iy]+100.6)/8.281)+0.3052*exp((v[ix][iy]+0.9941)/38.45));
jptau=1.46*jtau;
Ahf=0.99;
Ahs=1.0-Ahf;
mss = 1.0/(1.0+exp((-(v[ix][iy]+44.57))/9.871));


hss =1.0/(1+exp((v[ix][iy]+82.90)/6.086));
hssp=1.0/(1+exp((v[ix][iy]+89.1)/6.086));
if (iso == 1)
{		
	hss =1.0/(1+exp((v[ix][iy]+82.90+5)/6.086));             		
	hssp=1.0/(1+exp((v[ix][iy]+89.1+5)/6.086));
}
jss = hss;	

m[ix][iy] = mss-(mss-m[ix][iy])*exp(-dt/mtau);
hf[ix][iy]=hss-(hss-hf[ix][iy])*exp(-dt/hftau);
hs[ix][iy]=hss-(hss-hs[ix][iy])*exp(-dt/hstau);
hsp[ix][iy]=hssp-(hssp-hsp[ix][iy])*exp(-dt/hsptau);

h = Ahf*hf[ix][iy]+Ahs*hs[ix][iy];
hp=Ahf*hf[ix][iy]+Ahs*hsp[ix][iy];
j[ix][iy] = jss-(jss-j[ix][iy])*exp(-dt/jtau);
jp[ix][iy]=jss-(jss-jp[ix][iy])*exp(-dt/jptau);
	
finap=(1.0/(1.0+KmCaMK/CaMKa_cell));

mlss=1.0/(1.0+exp((-(v[ix][iy]+42.85))/5.264));
mltau = mtau;    
ml[ix][iy]=mlss-(mlss-ml[ix][iy])*exp(-dt/mltau);
    
hlss=1.0/(1.0+exp((v[ix][iy]+87.61)/7.488));
hlssp=1.0/(1.0+exp((v[ix][iy]+93.81)/7.488));
hltau=200.0;
hlptau=3.0*hltau;
hl[ix][iy]=hlss-(hlss-hl[ix][iy])*exp(-dt/hltau);
hlp[ix][iy]=hlssp-(hlssp-hlp[ix][iy])*exp(-dt/hlptau);
    
gnal = 0.0075;
finalp = finap;


ena = ((R*temp)/frdy)*log(nao/nai[ix][iy]);

    	
ina=gna*(v[ix][iy]-ena)*m[ix][iy]*m[ix][iy]*m[ix][iy]*((1.0-finap)*h*j[ix][iy]+finap*hp*jp[ix][iy]);
inal=gnal*(v[ix][iy]-ena)*ml[ix][iy]*((1.0-finalp)*hl[ix][iy]+finalp*hlp[ix][iy]);	

double EK;
EK=(R*temp/frdy)*log(ko/ki[ix][iy]);

double ass;     
ass=1.0/(1.0+exp((-(v[ix][iy]-14.34))/14.82));
double atau;
atau=1.0515/(1.0/(1.2089*(1.0+exp(-(v[ix][iy]-18.4099)/29.3814)))+3.5/(1.0+exp((v[ix][iy]+100.0)/29.3814))); 

a[ix][iy]=ass-(ass-a[ix][iy])*exp(-dt/atau);

double iss;     
iss=1.0/(1.0+exp((v[ix][iy]+43.94)/5.711));
double delta_epi=1.0;
double iftau,istau;     

iftau=4.562+1/(0.3933*exp((-(v[ix][iy]+100.0))/100.0)+0.08004*exp((v[ix][iy]+50.0)/16.59));
istau=23.62+1/(0.001416*exp((-(v[ix][iy]+96.52))/59.05)+1.780e-8*exp((v[ix][iy]+114.1)/8.079));
     
iftau*=delta_epi;
istau*=delta_epi;

double AiF,AiS;     
AiF=1.0/(1.0+exp((v[ix][iy]-213.6)/151.2));
AiS=1.0-AiF;
     
iF[ix][iy]=iss-(iss-iF[ix][iy])*exp(-dt/iftau);
iS[ix][iy]=iss-(iss-iS[ix][iy])*exp(-dt/istau);

double i_ito;     
i_ito=AiF*iF[ix][iy]+AiS*iS[ix][iy];
     
double assp;
assp=1.0/(1.0+exp((-(v[ix][iy]-24.34))/14.82));
ap[ix][iy]=assp-(assp-ap[ix][iy])*exp(-dt/atau);
     
double dti_develop,dti_recover;
dti_develop=1.354+1.0e-4/(exp((v[ix][iy]-167.4)/15.89)+exp(-(v[ix][iy]-12.23)/0.2154));
dti_recover=1.0-0.5/(1.0+exp((v[ix][iy]+70.0)/20.0));
     
double tiFp,tiSp;
tiFp=dti_develop*dti_recover*iftau;
tiSp=dti_develop*dti_recover*istau;

iFp[ix][iy]=iss-(iss-iFp[ix][iy])*exp(-dt/tiFp);
iSp[ix][iy]=iss-(iss-iSp[ix][iy])*exp(-dt/tiSp);

double ip;     
ip=AiF*iFp[ix][iy]+AiS*iSp[ix][iy];
     
double Gto=0.02;
double fItop;     
fItop=(1.0/(1.0+KmCaMK/CaMKa_cell));

Ito=Gto*(v[ix][iy]-EK)*((1.0-fItop)*a[ix][iy]*i_ito+fItop*ap[ix][iy]*ip);

double xkb=1.0/(1.0+exp(-(v[ix][iy]-14.48)/18.34));
double GKb=0.003;
if (iso == 1) 
	 GKb = 2.5*GKb;

IKb=GKb*xkb*(v[ix][iy]-EK);

double PNab=3.75e-10;
double vffrt = v[ix][iy]*frdy*frdy/(R*temp);
double vfrt = v[ix][iy]*frdy/(R*temp);
INab=PNab*vffrt*(nai[ix][iy]*exp(vfrt)-nao)/(exp(vfrt)-1.0);

double EKs=(R*temp/frdy)*log((ko+0.01833*nao)/(ki[ix][iy]+0.01833*nai[ix][iy]));
double xs1ss=1.0/(1.0+exp((-(v[ix][iy]+11.60))/8.932));
double txs1=817.3+1.0/(2.326e-4*exp((v[ix][iy]+48.28)/17.80)+0.001292*exp((-(v[ix][iy]+210.0))/230.0));
xs1[ix][iy]=xs1ss-(xs1ss-xs1[ix][iy])*exp(-dt/txs1);
double xs2ss=xs1ss;
double txs2=1.0/(0.01*exp((v[ix][iy]-50.0)/20.0)+0.0193*exp((-(v[ix][iy]+66.54))/31.0));
xs2[ix][iy]=xs2ss-(xs2ss-xs2[ix][iy])*exp(-dt/txs2);
double KsCa=1.0+0.6/(1.0+pow(3.8e-5/3.8e-5,1.4));
double GKs=0.0034;
if (iso == 1) GKs = 3.2*GKs;

IKs=GKs*KsCa*xs1[ix][iy]*xs2[ix][iy]*(v[ix][iy]-EKs);

double xrss=1.0/(1.0+exp((-(v[ix][iy]+8.337))/6.789));
double txrf=12.98+1.0/(0.3652*exp((v[ix][iy]-31.66)/3.869)+4.123e-5*exp((-(v[ix][iy]-47.78))/20.38));
double txrs=1.865+1.0/(0.06629*exp((v[ix][iy]-34.70)/7.355)+1.128e-5*exp((-(v[ix][iy]-29.74))/25.94));
double Axrf=1.0/(1.0+exp((v[ix][iy]+54.81)/38.21));
double Axrs=1.0-Axrf;
xrf[ix][iy]=xrss-(xrss-xrf[ix][iy])*exp(-dt/txrf);
xrs[ix][iy]=xrss-(xrss-xrs[ix][iy])*exp(-dt/txrs);
double xr=Axrf*xrf[ix][iy]+Axrs*xrs[ix][iy];
double rkr=1.0/(1.0+exp((v[ix][iy]+55.0)/75.0))*1.0/(1.0+exp((v[ix][iy]-10.0)/30.0));
double GKr=0.046; GKr = 1*GKr;

IKr=GKr*sqrt(ko/5.4)*xr*rkr*(v[ix][iy]-EK);

double xk1ss=1.0/(1.0+exp(-(v[ix][iy]+2.5538*ko+144.59)/(1.5692*ko+3.8115)));
double txk1=122.2/(exp((-(v[ix][iy]+127.2))/20.36)+exp((v[ix][iy]+236.8)/69.33));
xk1[ix][iy]=xk1ss-(xk1ss-xk1[ix][iy])*exp(-dt/txk1);
double rk1=1.0/(1.0+exp((v[ix][iy]+105.8-2.6*ko)/9.493));
double GK1=0.1908;
IK1=GK1*sqrt(ko)*rk1*xk1[ix][iy]*(v[ix][iy]-EK);

double zna = 1;
double zca = 2;
double k1p=949.5;
double k1m=182.4;
double k2p=687.2;
double k2m=39.4;
double k3p=1899.0;
double k3m=79300.0;
double k4p=639.0;
double k4m=40.0;
double Knai0=9.073;
double Knao0=27.78;
double delta=-0.1550;
double Knai=Knai0*exp((delta*v[ix][iy]*frdy)/(3.0*R*temp));
if (iso == 1) Knai = 0.7*Knai;
double Knao=Knao0*exp(((1.0-delta)*v[ix][iy]*frdy)/(3.0*R*temp));
double Kki=0.5;
double Kko=0.3582;
double MgADP=0.05;
double MgATP=9.8;
double Kmgatp=1.698e-7;
double H=1.0e-7;
double eP=4.2;
double Khp=1.698e-7;
double Knap=224.0;
double Kxkur=292.0;
double P=eP/(1.0+H/Khp+nai[ix][iy]/Knap+ki[ix][iy]/Kxkur);
double a1=(k1p*pow(nai[ix][iy]/Knai,3.0))/(pow(1.0+nai[ix][iy]/Knai,3.0)+pow(1.0+ki[ix][iy]/Kki,2.0)-1.0);
double b1=k1m*MgADP;
double a2=k2p;
double b2=(k2m*pow(nao/Knao,3.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
double a3=(k3p*pow(ko/Kko,2.0))/(pow(1.0+nao/Knao,3.0)+pow(1.0+ko/Kko,2.0)-1.0);
double b3=(k3m*P*H)/(1.0+MgATP/Kmgatp);
double a4=(k4p*MgATP/Kmgatp)/(1.0+MgATP/Kmgatp);
double b4=(k4m*pow(ki[ix][iy]/Kki,2.0))/(pow(1.0+nai[ix][iy]/Knai,3.0)+pow(1.0+ki[ix][iy]/Kki,2.0)-1.0);
double x1=a4*a1*a2+b2*b4*b3+a2*b4*b3+b3*a1*a2;
double x2=b2*b1*b4+a1*a2*a3+a3*b1*b4+a2*a3*b4;
double x3=a2*a3*a4+b3*b2*b1+b2*b1*a4+a3*a4*b1;
double x4=b4*b3*b2+a3*a4*a1+b2*a4*a1+b3*b2*a1;
double E1=x1/(x1+x2+x3+x4);
double E2=x2/(x1+x2+x3+x4);
double E3=x3/(x1+x2+x3+x4);
double E4=x4/(x1+x2+x3+x4);
double zk=1.0;
double JnakNa=3.0*(E1*a3-E2*b3);
double JnakK=2.0*(E4*b1-E3*a1);
double Pnak=30;

INaK=Pnak*(zna*JnakNa+zk*JnakK);

double dss=1.0/(1.0+exp((-(v[ix][iy]+3.940))/4.230));
if (iso == 1) dss=1.0/(1.0+exp((-(v[ix][iy]+3.940+14))/4.230));

double td=0.6+1.0/(exp(-0.05*(v[ix][iy]+6.0))+exp(0.09*(v[ix][iy]+14.0)));
d[ix][iy]=dss-(dss-d[ix][iy])*exp(-dt/td);

double fss=1.0/(1.0+exp((v[ix][iy]+19.58)/3.696));
if (iso == 1) fss=1.0/(1.0+exp((v[ix][iy]+19.58+8)/3.696));

double tff=7.0+1.0/(0.0045*exp(-(v[ix][iy]+20.0)/10.0)+0.0045*exp((v[ix][iy]+20.0)/10.0));
double tfs=1000.0+1.0/(0.000035*exp(-(v[ix][iy]+5.0)/4.0)+0.000035*exp((v[ix][iy]+5.0)/6.0));
double Aff=0.6;
double Afs=1.0-Aff;
ff[ix][iy]=fss-(fss-ff[ix][iy])*exp(-dt/tff);
fs[ix][iy]=fss-(fss-fs[ix][iy])*exp(-dt/tfs);
double f=Aff*ff[ix][iy]+Afs*fs[ix][iy];
double fcass=fss;
double tfcaf=7.0+1.0/(0.04*exp(-(v[ix][iy]-4.0)/7.0)+0.04*exp((v[ix][iy]-4.0)/7.0));
double tfcas=100.0+1.0/(0.00012*exp(-v[ix][iy]/3.0)+0.00012*exp(v[ix][iy]/7.0));
double Afcaf=0.3+0.6/(1.0+exp((v[ix][iy]-10.0)/10.0));
double Afcas=1.0-Afcaf;
fcaf[ix][iy]=fcass-(fcass-fcaf[ix][iy])*exp(-dt/tfcaf);
fcas[ix][iy]=fcass-(fcass-fcas[ix][iy])*exp(-dt/tfcas);
double fca=Afcaf*fcaf[ix][iy]+Afcas*fcas[ix][iy];
double tjca=75.0;
jca[ix][iy]=fcass-(fcass-jca[ix][iy])*exp(-dt/tjca);
double tffp=2.5*tff;
ffp[ix][iy]=fss-(fss-ffp[ix][iy])*exp(-dt/tffp);
double fp=Aff*ffp[ix][iy]+Afs*fs[ix][iy];
double tfcafp=2.5*tfcaf;
fcafp[ix][iy]=fcass-(fcass-fcafp[ix][iy])*exp(-dt/tfcafp);
double fcap=Afcaf*fcafp[ix][iy]+Afcas*fcas[ix][iy];

//for plotting ICaL only//

double Kmn=0.002;
double k2n=1000.0;
double km2n=jca[ix][iy]*1.0;
double anca=1.0/(k2n/km2n+pow(1.0+Kmn/cadyad,4.0));
double nca_print=anca*k2n/km2n-(anca*k2n/km2n-nca_print)*exp(-km2n*dt);
double fICaLp=(1.0/(1.0+KmCaMK/CaMKa_cell));
double PCa=0.0001;
if (iso == 1) PCa = 2.5*PCa;
double PCap=1.1*PCa;
double PCaNa=0.00125*PCa;
double PCaK=3.574e-4*PCa;
double PCaNap=0.00125*PCap;
double PCaKp=3.574e-4*PCap;
double PhiCaL=4.0*vffrt*(1e-3*cadyad*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
double PhiCaNa=1.0*vffrt*(0.75*nai[ix][iy]*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
double PhiCaK=1.0*vffrt*(0.75*ki[ix][iy]*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);

double ICaL_print=(1.0-fICaLp)*PCa*PhiCaL*d[ix][iy]*(f*(1.0-nca_print)+jca[ix][iy]*fca*nca_print)+fICaLp*PCap*PhiCaL*d[ix][iy]*(fp*(1.0-nca_print)+jca[ix][iy]*fcap*nca_print);
double ICaNa_print=(1.0-fICaLp)*PCaNa*PhiCaNa*d[ix][iy]*(f*(1.0-nca_print)+jca[ix][iy]*fca*nca_print)+fICaLp*PCaNap*PhiCaNa*d[ix][iy]*(fp*(1.0-nca_print)+jca[ix][iy]*fcap*nca_print);
double ICaK_print=(1.0-fICaLp)*PCaK*PhiCaK*d[ix][iy]*(f*(1.0-nca_print)+jca[ix][iy]*fca*nca_print)+fICaLp*PCaKp*PhiCaK*d[ix][iy]*(fp*(1.0-nca_print)+jca[ix][iy]*fcap*nca_print);

double ar = 0.0011;
double	al = 0.01;
double	pi = 3.142;
double	ageo = 2*pi*ar*ar + 2*pi*ar*al;
double	acap = 2*ageo;
double vcell = 1000*pi*ar*ar*al;	
double	vmyo = 0.68*vcell;
double vds = 2e-13;

double	vnsr = 0.0552*vcell;
double	vjsr = 0.0048*vcell;
double	vss = 0.01*vcell;
double	vjsr_dyad = vjsr/Ntotdyads;
double	vnsr_dyad = vnsr/Ntotdyads;
double	vcyt_dyad = vmyo/Ntotdyads;
double	vss_dyad = vss/Ntotdyads;
double	vol_ds = 2e-13;

double nodhpr,jrel,noryr,nmodeca_total,nivf_total,nivs_total;
double factivedyad;

ilca = nodhpr = jrel = noryr = nmodeca_total = nivf_total = nivs_total = 0;
ilcana=ilcak=0;
inaca = icab = ipca = icat = inacass = 0;
cai = casub = cadyad = jsr = nsr = 0;
CaMKa_cell = CaMKb_cell = CaMKt_cell = 0;
factivedyad  = 0;
	
for (int i=0; i<Ndyads;i++)
{
Kmn=0.002;
k2n=1000.0;
km2n=jca[ix][iy]*1.0;
anca=1.0/(k2n/km2n+pow(1.0+Kmn/(0.001*cads[ix][iy][i]),4.0));
nca[ix][iy][i]=anca*k2n/km2n-(anca*k2n/km2n-nca[ix][iy][i])*exp(-km2n*dt);     
int nLCC_dyad_np = 0;
int nLCC_dyad_p = 0;         
              
for (int k = 0;k<Ndhpr;k++)
{
	if ( ndrand48(pRD) > (1.0/(1.0+KmCaMK/CaMKa)) ) //Channel is not CaMK phosphorylated
	{
           if ( ndrand48(pRD) > nca[ix][iy][i] ) //Channel is not in Ca-CaMK dependent state
           {
              if (ndrand48(pRD) <d[ix][iy]*f ) //Channel is Open
              {
                 nLCC_dyad_np ++;
              }                                    
           }           
           else                                    //Channel is in Ca-CaMK dependent state
           {                                                   
              if (ndrand48(pRD) < d[ix][iy]*jca[ix][iy]*fca)
              {
                 nLCC_dyad_np ++;
              }
           }                                          
        }
        else                                          //Channel is CaMK phosphorylated
        {
           if ( ndrand48(pRD) > nca[ix][iy][i] ) //Channel is not in Ca-CaMK dependent state
           {
              if (ndrand48(pRD) <d[ix][iy]*fp ) //Channel is Open
              {
                 nLCC_dyad_p ++;
              }                                    
           }           
           else                                    //Channel is in Ca-CaMK dependent state
           {                                                   
              if (ndrand48(pRD) < d[ix][iy]*jca[ix][iy]*fcap)
              {
                 nLCC_dyad_p ++;
              }
           }                                          
        }

}   

//	if ((ix == N_x/2) && (iy == N_y/2)) 
//		f2out<<t<<"	"<<(nLCC_dyad_p + nLCC_dyad_np)<<endl;

PhiCaL=4.0*vffrt*(1e-3*cads[ix][iy][i]*0.01*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
PhiCaNa=1.0*vffrt*(0.75*nai[ix][iy]*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
PhiCaK=1.0*vffrt*(0.75*ki[ix][iy]*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);
PCa=0.0001;     
if (iso == 1) PCa = 2.5*PCa;	
PCap=1.1*PCa;
PCaNa=0.00125*PCa;
PCaK=3.574e-4*PCa;
PCaNap=0.00125*PCap;
PCaKp=3.574e-4*PCap;

double ilca_dyad = PCa*PhiCaL*(nLCC_dyad_np*1.0/Ndhpr) + PCap*PhiCaL*(nLCC_dyad_p*1.0/Ndhpr);	//uA/uF
double icana_dyad = PCaNa*PhiCaNa*(nLCC_dyad_np*1.0/Ndhpr) + PCaNap*PhiCaNa*(nLCC_dyad_p*1.0/Ndhpr);	//uA/uF
double icak_dyad = PCaK*PhiCaK*(nLCC_dyad_np*1.0/Ndhpr) + PCaKp*PhiCaK*(nLCC_dyad_p*1.0/Ndhpr);	//uA/uF

ilca += ilca_dyad/Ndyads;	//uA/uF
ilcana += icana_dyad/Ndyads;
ilcak += icak_dyad/Ndyads;
nodhpr +=  nLCC_dyad_np + nLCC_dyad_p;

double jlca_dyad = -acap*ilca_dyad*1e3/(2*vds*frdy*Ntotdyads);	//uM/ms

double Km = 5;
double Kmcsqn = 150;
double dryr = 4;
int nryropen = 0;
nactive[ix][iy][i] = 0;

for (int k=0; k<Nryr; k++)
{
	if((ryrstate[ix][iy][i][k] == 1) || (ryrstate[ix][iy][i][k] == 3)) nryropen++;
}

if (  (1.0*nryropen/Nryr)>0.2) {trel[ix][iy][i] = t; nactive[ix][iy][i] = 1;}
	
double  kC1O1 = 3.*(pow(cads[ix][iy][i],4)/(pow(cads[ix][iy][i],4) + pow(Km,4)));
double	kO1C1 = 0.5;	
double	kC2O2 = 3.*(pow(cads[ix][iy][i],4)/(pow(cads[ix][iy][i],4) + pow(Kmcsqn,4)));
double	kO2C2 = kO1C1;

//calsequestrin
double	csqn = csqnbar*pow(kmcsqn,8)/(pow(cajsr[ix][iy][i],8) + pow(kmcsqn,8) );
double	csqnca = csqnbar - csqn;

double kC1C2 = 1*(csqn/csqnbar);

double tref;

tref = 0.9*bcl;
double kC2C1 = 1*(csqnca/csqnbar)*(t-trel[ix][iy][i]-tref);
		
double	kO1O2 = kC1C2;
double	kO2O1 = kC2C1;

int nryrc1,nryro1,nryrc2,nryro2;
nryrc1 = nryro1 = nryrc2 = nryro2 = nryropen = 0;

for ( int k=0;k<Nryr;k++)
{		
	double randomnumber = ndrand48(pRD);
	switch (ryrstate[ix][iy][i][k])
	{
		case 0:
		if (randomnumber <= kC1O1*dt)
		{ryrstate[ix][iy][i][k] = 1; nryro1++; break;}
		if ((randomnumber>kC1O1*dt) && (randomnumber<= (kC1O1+kC1C2)*dt))
		{ryrstate[ix][iy][i][k]=2; nryrc2++;break;}
		if ((randomnumber> (kC1O1+kC1C2)*dt) && (randomnumber <= 1))
		{ryrstate[ix][iy][i][k]=0; nryrc1++;break;}

		case 1:
		if (randomnumber <= kO1C1*dt)
		{ryrstate[ix][iy][i][k] = 0; nryrc1++; break;}
		if ((randomnumber>kO1C1*dt) && (randomnumber<=(kO1C1+kO1O2)*dt))
		{ryrstate[ix][iy][i][k] = 3; nryro2++; break;}
		if ((randomnumber>(kO1C1+kO1O2)*dt) && (randomnumber<=1))
		{ryrstate[ix][iy][i][k] = 1; nryro1++; break;}
		
		case 2:
		if (randomnumber <= kC2C1*dt)
		{ryrstate[ix][iy][i][k] = 0; nryrc1++; break;}
		if ((randomnumber>kC2C1*dt) && (randomnumber<=(kC2C1+kC2O2)*dt))
		{ryrstate[ix][iy][i][k] = 3; nryro2++; break;}
		if ((randomnumber>(kC2C1+kC2O2)*dt) && (randomnumber<=1))
		{ryrstate[ix][iy][i][k] = 2; nryrc2++; break;}

		case 3:
		if (randomnumber <= kO2C2*dt)
		{ryrstate[ix][iy][i][k] = 2; nryrc2++; break;}
		if ((randomnumber>kO2C2*dt) && (randomnumber<=(kO2C2+kO2O1)*dt))
		{ryrstate[ix][iy][i][k] = 1; nryro1++; break;}
		if ((randomnumber>(kO2C2+kO2O1)*dt) && (randomnumber<=1))
		{ryrstate[ix][iy][i][k] = 3; nryro2++; break;}
	}
}

//	nryropen = nryro1 + nryro2;
nryropen = nryro1;

//if (t> (beats-3)*bcl && t<(beats-3)*bcl+20) nryropen = Nryr;

//if (t>(beats-5)*bcl) Dx = 0;


Poryr[ix][iy][i] =  1.0*nryropen/Nryr;

double jrel_dyad = dryr*nryropen*(cajsr[ix][iy][i]*1e3-cads[ix][iy][i]); //uM/ms	
jrel += jrel_dyad*(vds/vmyo)*(Ntotdyads/Ndyads); //mM/s

noryr += nryropen;

double tauefflux = 7e-4;
double jcadiff_ds_ss = (cads[ix][iy][i]-cass[ix][iy][i]*1e3)/tauefflux;
double jefflux = jcadiff_ds_ss;

cads[ix][iy][i] =  (jrel_dyad + jlca_dyad + cass[ix][iy][i]*1e3/tauefflux)*tauefflux;

double	taudiff = 0.1;
double	bsubspace = 1/(1 + ((bsrbar*kmbsr)/(pow((kmbsr+cass[ix][iy][i]),2))) + ((bslbar*kmbsl)/(pow((kmbsl+cass[ix][iy][i]),2))));
double jcadiff_ss_cyt = (cass[ix][iy][i]-cacyt[ix][iy][i])/taudiff;

// local submembrane space concentration
double inacass_dyad;
cass[ix][iy][i] = cass[ix][iy][i] + (-bsubspace*(((-2*inacass_dyad)*(acap/(Ntotdyads*zca*frdy*vss_dyad))) + jcadiff_ss_cyt - jcadiff_ds_ss*1e-3*(vds/vss_dyad) ))*dt;
double taurefill = 20;

double jrefill = (cansr[ix][iy][i] - cajsr[ix][iy][i])/taurefill;

double	bjsr = 1/( 1 + (csqnbar*kmcsqn)/pow((kmcsqn + cajsr[ix][iy][i]),2));
		
cajsr[ix][iy][i] = cajsr[ix][iy][i] + bjsr*(-(jrel_dyad*(vds/vjsr_dyad))/1000. + jrefill)*dt;
double Jupnp=0.004375*cacyt[ix][iy][i]/(cacyt[ix][iy][i]+0.00092);
double Jupp=2.75*0.004375*cacyt[ix][iy][i]/(cacyt[ix][iy][i]+0.00092-0.00017);
if (iso == 1)
{
	Jupnp=0.004375*cacyt[ix][iy][i]/(cacyt[ix][iy][i]+0.54*0.00092);
    	Jupp=2.75*0.004375*cacyt[ix][iy][i]/(cacyt[ix][iy][i]+0.54*(0.00092-0.00017));
}

double fJupp=(1.0/(1.0+KmCaMK/CaMKa));    
double ileak=0.0039375*cansr[ix][iy][i]/15.0;
double iup=((1.0-fJupp)*Jupnp+fJupp*Jupp);

cansr[ix][iy][i] = cansr[ix][iy][i] + (iup - ileak - jrefill*(vjsr_dyad/vnsr_dyad))*dt;	

// local CaMK bound, active and trapped concentration
CaMKb = CaMKo*(1.0-CaMKt[ix][iy][i] )/(1.0+KmCaM/cass[ix][iy][i]);
CaMKa = CaMKb + CaMKt[ix][iy][i];
CaMKt[ix][iy][i] += dt*(aCaMK*CaMKb*(CaMKb+CaMKt[ix][iy][i])-bCaMK*CaMKt[ix][iy][i]);


double icab_dyad,ipca_dyad,inaca_dyad;
double bmyo;

bmyo = 1/(1 + ((trpnbar*kmtrpn)/(pow((kmtrpn+cacyt[ix][iy][i]),2))) + ((cmdnbar*kmcmdn)/(pow((kmcmdn+cacyt[ix][iy][i]),2))));

// local myoplasmic Ca concentration
cacyt[ix][iy][i] = cacyt[ix][iy][i] + (-bmyo*(((icab_dyad+ipca_dyad-2*inaca_dyad)*(acap/(Ntotdyads*zca*frdy*vcyt_dyad ))) + (iup-ileak)*(vnsr_dyad/vcyt_dyad) - jcadiff_ss_cyt*(vss_dyad/vcyt_dyad) ))*dt;

// calcium currents
double kna1=15.0;
double kna2=5.0;
double kna3=88.12;
double kasymm=12.5;
double wna=6.0e4;
double wca=6.0e4;
double wnaca=5.0e3;
double kcaon=1.5e6;
double kcaoff=5.0e3;
double qna=0.5224;
double qca=0.1670;
double hca=exp((qca*v[ix][iy]*frdy)/(R*temp));
double hna=exp((qna*v[ix][iy]*frdy)/(R*temp));
double h1=1+nai[ix][iy]/kna3*(1+hna);
double h2=(nai[ix][iy]*hna)/(kna3*h1);
double h3=1.0/h1;
double h4=1.0+nai[ix][iy]/kna1*(1+nai[ix][iy]/kna2);
double h5=nai[ix][iy]*nai[ix][iy]/(h4*kna1*kna2);
double h6=1.0/h4;
double h7=1.0+nao/kna3*(1.0+1.0/hna);
double h8=nao/(kna3*hna*h7);
double h9=1.0/h7;
double h10=kasymm+1.0+nao/kna1*(1.0+nao/kna2);
double h11=nao*nao/(h10*kna1*kna2);
double h12=1.0/h10;
double k1=h12*cao*kcaon;
double k2=kcaoff;
double k3p=h9*wca;
double k3pp=h8*wnaca;
double k3=k3p+k3pp;
double k4p=h3*wca/hca;
double k4pp=h2*wnaca;
double k4=k4p+k4pp;
double k5=kcaoff;
double k6=h6*cacyt[ix][iy][i]*kcaon;
double k7=h5*h2*wna;
double k8=h8*h11*wna;
double x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
double x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
double x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
double x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
double E1=x1/(x1+x2+x3+x4);
double E2=x2/(x1+x2+x3+x4);
double E3=x3/(x1+x2+x3+x4);
double E4=x4/(x1+x2+x3+x4);
double KmCaAct=150.0e-6;
double allo=1.0/(1.0+pow(KmCaAct/cacyt[ix][iy][i],2.0));
double zna=1.0;
double JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
double JncxCa=E2*k2-E1*k1;
double Gncx=0.0008;


inaca_dyad=0.8*Gncx*allo*(zna*JncxNa+zca*JncxCa);

h1=1+nai[ix][iy]/kna3*(1+hna);
h2=(nai[ix][iy]*hna)/(kna3*h1);
h3=1.0/h1;
h4=1.0+nai[ix][iy]/kna1*(1+nai[ix][iy]/kna2);
h5=nai[ix][iy]*nai[ix][iy]/(h4*kna1*kna2);
h6=1.0/h4;
h7=1.0+nao/kna3*(1.0+1.0/hna);
h8=nao/(kna3*hna*h7);
h9=1.0/h7;
h10=kasymm+1.0+nao/kna1*(1+nao/kna2);
h11=nao*nao/(h10*kna1*kna2);
h12=1.0/h10;
k1=h12*cao*kcaon;
k2=kcaoff;
k3p=h9*wca;
k3pp=h8*wnaca;
k3=k3p+k3pp;
k4p=h3*wca/hca;
k4pp=h2*wnaca;
k4=k4p+k4pp;
k5=kcaoff;
k6=h6*cass[ix][iy][i]*kcaon;
k7=h5*h2*wna;
k8=h8*h11*wna;
x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
E1=x1/(x1+x2+x3+x4);
E2=x2/(x1+x2+x3+x4);
E3=x3/(x1+x2+x3+x4);
E4=x4/(x1+x2+x3+x4);
KmCaAct=150.0e-6;
allo=1.0/(1.0+pow(KmCaAct/cass[ix][iy][i],2.0));
JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
JncxCa=E2*k2-E1*k1;

inacass_dyad=0.2*Gncx*allo*(zna*JncxNa+zca*JncxCa);

double PCab=2.5e-8;
icab_dyad=PCab*4.0*vffrt*(cacyt[ix][iy][i]*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
double GpCa=0.0005;
ipca_dyad=GpCa*cacyt[ix][iy][i]/(0.0005+cacyt[ix][iy][i]);

inaca += (inaca_dyad)/Ndyads;
inacass += (inacass_dyad)/Ndyads;
ipca += (ipca_dyad)/Ndyads;
icab += (icab_dyad)/Ndyads;

}

for (int i = 0; i<Ndyads; i++) //xfer ca from reaction to diffusion space
{
	int ix,iy,iz;
	iz= i/(Nx*Ny);    				
	iy = (i - iz*(Nx*Ny))/Nx; 			
	ix = i -iz*(Nx*Ny) - iy*Ny;		

	cacytdiff[(Nvox*iz)*Nx_diff*Ny_diff + (Nvox*iy)*Nx_diff + (Nvox*ix)] = cacyt[ix][iy][i];
  	cansrdiff[(Nvox*iz)*Nx_diff*Ny_diff + (Nvox*iy)*Nx_diff + (Nvox*ix)] = cansr[ix][iy][i];
	cassdiff[ (Nvox*iz)*Nx_diff*Ny_diff + (Nvox*iy)*Nx_diff + (Nvox*ix)] = cass[ix][iy][i];
}


for (int i = 0; i<Nx_diff*Ny_diff*Nz_diff; i++)
{
	// temp variables to compute ca in diffusion space
	cacyttemp[i] = cacytdiff[i]; 
	cansrtemp[i] = cansrdiff[i]; 
	casstemp[i] = cassdiff[i];
	
	int iz,iy,ix;

	iz= i/(Nx_diff*Ny_diff);    			//3D mapping
	iy = (i - iz*(Nx_diff*Ny_diff))/Nx_diff; 		//3D mapping
	ix = i -iz*(Nx_diff*Ny_diff) - iy*Ny_diff;	//3D mapping

	double bsubspace = 1/(1 + ((bsrbar*kmbsr)/(pow((kmbsr+cassdiff[i]),2))) + ((bslbar*kmbsl)/(pow((kmbsl+cassdiff[i]),2))));
	double bmyo = 1/(1 + ((trpnbar*kmtrpn)/(pow((kmtrpn+cacytdiff[i]),2))) + ((cmdnbar*kmcmdn)/(pow((kmcmdn+cacytdiff[i]),2))));

	if (ix>0 && ix<Nx_diff-1)
	{ 
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*(cacytdiff[i-1] + cacytdiff[i+1] -2*cacytdiff[i])*dt/(dx*dx);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*(cassdiff[i-1] + cassdiff[i+1] - 2*cassdiff[i])*dt/(dx*dx);
		cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-1] + cansrdiff[i+1] -2*cansrdiff[i])*dt/(dx*dx);
	}

	if (iy>0 && iy<Ny_diff-1)
	{ 
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*(cacytdiff[i-Nx_diff] + cacytdiff[i+Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*(cassdiff[i-Nx_diff] + cassdiff[i+Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
		cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-Nx_diff] + cansrdiff[i+Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
	}
	
	if (iz>0 && iz<Nz_diff-1)
	{ 
		cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*(cacytdiff[i-Nx_diff*Ny_diff] + cacytdiff[i+Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
		casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*(cassdiff[i-Nx_diff*Ny_diff] + cassdiff[i+Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
		cansrtemp[i] = cansrtemp[i] + Dsr*(cansrdiff[i-Nx_diff*Ny_diff] + cansrdiff[i+Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
	}

	if (ix==0 && ix!=(Nx_diff-1))
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i+1] -2*cacytdiff[i])*dt/(dx*dx);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i+1] - 2*cassdiff[i])*dt/(dx*dx);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+1] -2*cansrdiff[i])*dt/(dx*dx);
	}
	if (ix == Nx_diff-1 && ix != 0)
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i-1] -2*cacytdiff[i])*dt/(dx*dx);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i-1] - 2*cassdiff[i])*dt/(dx*dx);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-1] -2*cansrdiff[i])*dt/(dx*dx);
	}
	if (iy == 0 && iy != (Ny_diff-1))
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i+Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i+Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
	}
	if (iy == Ny_diff-1 && iy != 0)
	{
		cacyttemp[i] = cacyttemp[i] + Dcyt*bmyo*( 2*cacytdiff[i-Nx_diff] -2*cacytdiff[i])*dt/(dy*dy);
		casstemp[i] = casstemp[i] + Dcyt*bsubspace*( 2*cassdiff[i-Nx_diff] - 2*cassdiff[i])*dt/(dy*dy);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-Nx_diff] -2*cansrdiff[i])*dt/(dy*dy);
	}

	if (iz == 0 && iz != (Nz_diff-1))
	{
		cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*( 2*cacytdiff[i+Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
		casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*( 2*cassdiff[i+Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i+Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
	}
	if (iz == Nz_diff-1 && iz != 0)
	{
		cacyttemp[i] = cacyttemp[i] + 2*Dcyt*bmyo*( 2*cacytdiff[i-Nx_diff*Ny_diff] -2*cacytdiff[i])*dt/(dz*dz);
		casstemp[i] = casstemp[i] + 2*Dcyt*bsubspace*( 2*cassdiff[i-Nx_diff*Ny_diff] - 2*cassdiff[i])*dt/(dz*dz);
		cansrtemp[i] = cansrtemp[i] + Dsr*( 2*cansrdiff[i-Nx_diff*Ny_diff] -2*cansrdiff[i])*dt/(dz*dz);
	}
	
	}

	for (int i = 0; i<Nx_diff*Ny_diff*Nz_diff; i++)
	{
		cacytdiff[i] = cacyttemp[i]; cassdiff[i] = casstemp[i]; cansrdiff[i] = cansrtemp[i];
	}


	for (int i = 0; i<Ndyads; i++) //xfer ca from diffusion to reaction space
	{
		int ix,iy,iz;
	    	iz= i/(Nx*Ny);    				
	     	iy = (i - iz*(Nx*Ny))/Nx; 			
	     	ix = i -iz*(Nx*Ny) - iy*Ny;		

		cacyt[ix][iy][i] = cacytdiff[(Nvox*iz)*Nx_diff*Ny_diff + (Nvox*iy)*Nx_diff + (Nvox*ix)];
  		cansr[ix][iy][i] = cansrdiff[(Nvox*iz)*Nx_diff*Ny_diff + (Nvox*iy)*Nx_diff + (Nvox*ix)];
		cass[ix][iy][i] = cassdiff[ (Nvox*iz)*Nx_diff*Ny_diff + (Nvox*iy)*Nx_diff + (Nvox*ix)];
	}


for (int i=0; i<Ndyads; i++)
{
	cai += cacyt[ix][iy][i]/Ndyads; 
	casub += cass[ix][iy][i]/Ndyads;
	cadyad += cads[ix][iy][i]/Ndyads;
	jsr += cajsr[ix][iy][i]/Ndyads;
	nsr += cansr[ix][iy][i]/Ndyads;
	factivedyad += nactive[ix][iy][i]/Ndyads;
	CaMKa_cell += CaMKa/Ndyads;
       CaMKb_cell += CaMKb/Ndyads;
       CaMKt_cell += CaMKt[ix][iy][i]/Ndyads;
}

double dnai = -dt*(naiont*acap)/(vmyo*zna*frdy);
nai[ix][iy] = dnai + nai[ix][iy];
double dki = -dt*((kiont)*acap)/(vmyo*zk*frdy);
ki[ix][iy] = dki + ki[ix][iy];

//if ( (ix>3*N_x/8 && ix<5*N_x/8) && (iy>3*N_y/8 && iy<5*N_y/8) ) istim = ist; else istim = 0; 
if ( iy == 1 ) istim = ist; else istim = 0; 

naiont = ina+inal+INab+ilcana+3*INaK+3*inaca+3*inacass;
kiont = Ito + IKr + IKs + IK1 + ilcak + IKb - 2*INaK + istim;
caiont = ilca+icab+ipca-2*inaca-2*inacass;		
it = naiont + kiont + caiont;

v[ix][iy] += -it*dt;

} 
